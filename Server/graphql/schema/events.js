exports.Event = `
    type Event {
        _id: ID!
        title: String!
        description: String!
        price: Float!
        date: String!
        creator: User!
    }
`;

exports.EventInput = `
    input EventInput {
        title: String!
        description: String!
        price: Float!
        date: String!
    }
`;

exports.EventQueries = `
    events: [Event!]!
`;

exports.EventMutations = `
    createEvent(eventInput: EventInput): Event
`;