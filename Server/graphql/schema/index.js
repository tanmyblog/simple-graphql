const { buildSchema } = require('graphql');
const userSchemas = require('./users');
const eventSchemas = require('./events');
const bookingSchemas = require('./booking');

module.exports = buildSchema(`
    ${bookingSchemas.Booking}

    ${eventSchemas.Event}

    ${userSchemas.User}

    ${bookingSchemas.AuthData}

    ${eventSchemas.EventInput}
    
    ${userSchemas.UserInput}
    
    type RootQuery {
        ${eventSchemas.EventQueries}
        ${bookingSchemas.BookingQueries}
        ${userSchemas.UserQueries}
    }

    type RootMutation {
        ${eventSchemas.EventMutations}
        ${userSchemas.UserMutations}
        ${bookingSchemas.BookingMutations}
    }

    schema {
        query: RootQuery
        mutation: RootMutation
    }
`);