exports.Booking = `
    type Booking {
        _id: ID!
        event: Event!
        user: User!
        createdAt: String!
        updatedAt: String!
    }
`;
exports.AuthData = `
    type AuthData {
        userId: ID!
        token: String!
        tokenExpiration: Int!
    }
`;

exports.BookingQueries = `
    bookings: [Booking!]!
`;

exports.BookingMutations = `
    bookEvent(eventId: ID!): Booking!
    cancelBooking(bookingId: ID!): Event!
`;