exports.User = `
    type User {
        _id: ID!
        email: String!
        password: String
        createdEvents: [Event!]
    }
`;

exports.UserInput = `
    input UserInput {
        email: String!
        password: String!
    }
`;

exports.UserQueries = `
    login(email: String!, password: String!): AuthData!
`;

exports.UserMutations = `
    createUser(userInput: UserInput): User
    
`;