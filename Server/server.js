const express = require('express');
const graphqlHttp = require('express-graphql');
const cors = require('cors');
const connectDB = require('./config/db');
const isAuth = require('./middleware/is-auth');

const graphQlSchema = require('./graphql/schema/index');
const graphQlResolvers = require('./graphql/resolvers/index');

const app = express();
const PORT = process.env.PORT || 9090;

// Connect DB
connectDB();

app.use(express.json({ extended: false }));
app.use(cors());
app.use(isAuth);

app.use('/graphql', graphqlHttp({
    schema: graphQlSchema,
    rootValue: graphQlResolvers,
    graphiql: true
}))

app.listen(PORT, () => console.log(`Server started on port ${PORT}`));