import React, { Component, Fragment } from 'react';
import Modal from '../components/modal';
import Backdrop from '../components/backdrop';
import AuthContext from '../context/auth-context';
import EventList from '../components/events/event-list';
import './styles.css';

export class EventPage extends Component {

    static contextType = AuthContext;

    constructor(props) {
        super(props)

        this.state = {
            creating: false,
            events: [],
        }

        this.titleRef = React.createRef();
        this.priceRef = React.createRef();
        this.dateRef = React.createRef();
        this.descRef = React.createRef();
    }

    componentDidMount() {
        this.fetchEvents();
    }

    createEventHandler = () => {
        this.setState({ creating: true });
    }

    confirmHandler = () => {
        const title = this.titleRef.current.value;
        const price = this.priceRef.current.value;
        const date = this.dateRef.current.value;
        const description = this.descRef.current.value;

        if (title.trim().length === 0 ||
            price <= 0 ||
            date.trim().length === 0 ||
            description.trim().length === 0
        ) { return; }

        let requestBody = {
            query: `
                mutation {
                    createEvent(eventInput: {title: "${title}", description: "${description}", price: ${parseFloat(price)}, date: "${date}"}) {
                        _id
                        title
                        description
                        date
                        price
                        creator {
                            _id
                            email
                        }
                    }
                }
            `
        };

        fetch('http://localhost:9090/graphql', {
            method: 'POST',
            body: JSON.stringify(requestBody),
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${this.context.token}`
            }
        })
            .then(res => {
                if (res.status !== 200 && res.status !== 201) {
                    throw new Error('Failed!');
                }
                return res.json();
            })
            .then(resData => {

            })
            .catch(err => {
                console.log(err);
            });

        this.setState({ creating: false });
    }

    cancelHandler = () => {
        this.setState({ creating: false, selectedEvent: null });
    }

    fetchEvents = () => {
        const requestBody = {
            query: `
              query {
                events {
                  _id
                  title
                  description
                  date
                  price
                  creator {
                    _id
                    email
                  }
                }
              }
            `
        };

        fetch('http://localhost:9090/graphql', {
            method: 'POST',
            body: JSON.stringify(requestBody),
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${this.context.token}`
            }
        })
            .then(res => {
                if (res.status !== 200 && res.status !== 201) {
                    throw new Error('Failed!');
                }
                return res.json();
            })
            .then(resData => {
                this.setState({ events: resData.data.events });
            })
            .catch(err => {
                console.log(err);
            });
    }

    showDetailHandler = eventId => {
        this.setState(prevState => {
            const selectedEvent = prevState.events.find(e => e._id === eventId);
            return { selectedEvent: selectedEvent };
        });
    };

    render() {
        const { creating, selectedEvent } = this.state;
        return (
            <Fragment>
                {creating &&
                    <Fragment>
                        <Backdrop />
                        <Modal title="Add event"
                            canCancel
                            canConfirm
                            onCancel={this.cancelHandler}
                            onConfirm={this.confirmHandler}
                        >
                            <form>
                                <div className="form-control">
                                    <label htmlFor="title">Title</label>
                                    <input type="text" id="title" ref={this.titleRef} />
                                </div>

                                <div className="form-control">
                                    <label htmlFor="price">Price</label>
                                    <input type="number" id="price" ref={this.priceRef} />
                                </div>

                                <div className="form-control">
                                    <label htmlFor="date">Date</label>
                                    <input type="datetime-local" id="date" ref={this.dateRef} />
                                </div>

                                <div className="form-control">
                                    <label htmlFor="description">Description</label>
                                    <textarea id="description" rows="5" ref={this.descRef} />
                                </div>
                            </form>
                        </Modal>
                    </Fragment>
                }
                <div className="event-content">
                    <button className="btn" onClick={() => this.createEventHandler()}>
                        create event
                    </button>
                </div>

                {selectedEvent &&
                    <Fragment>
                        <Backdrop />
                        <Modal
                            title={selectedEvent.title}
                            canCancel
                            canConfirm
                            onCancel={this.cancelHandler}
                            onConfirm={this.bookEventHandler}
                            confirmText="Book"
                        >
                            <div>
                                <h1>{selectedEvent.title}</h1>
                                <h2>
                                    ${selectedEvent.price} -{' '}
                                    {new Date(selectedEvent.date).toLocaleDateString()}
                                </h2>
                                <p>{selectedEvent.description}</p>
                            </div>
                        </Modal>
                    </Fragment>
                }

                <EventList
                    events={this.state.events}
                    authUserId={this.context.userId}
                    onViewDetail={this.showDetailHandler}
                />

            </Fragment>
        )
    }
}

export default EventPage
