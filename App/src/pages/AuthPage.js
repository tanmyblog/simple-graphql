import React, { Component } from 'react';
import AuthContext from '../context/auth-context';
import './styles.css';

export class AuthPage extends Component {

    static contextType = AuthContext;

    constructor(props) {
        super(props);
        this.state = {
            isLogin: true,
        };
        this.emailRef = React.createRef();
        this.passwordRef = React.createRef();
    }

    switchHandler = () => {
        this.setState(prevState => {
            return { isLogin: !prevState.isLogin }
        });
    }

    handleSubmit = event => {
        event.preventDefault();
        const email = this.emailRef.current.value;
        const password = this.passwordRef.current.value;

        if (email.trim().length === 0 || password.trim().length === 0) return;

        let requestBody = {
            query: `
                query {
                    login(email: "${email}", password: "${password}") {
                        userId
                        token
                        tokenExpiration
                    }
                }
            `
        };

        if (!this.state.isLogin) {
            requestBody = {
                query: `
                    mutation {
                        createUser(userInput: {email: "${email}", password: "${password}"}) {
                            _id
                            email
                        }
                    }
                `
            };
        }

        fetch('http://localhost:9090/graphql', {
            method: 'POST',
            body: JSON.stringify(requestBody),
            headers: {
                'Content-Type': 'application/json',
            }
        })
            .then(res => {
                if (res.status !== 200 && res.status !== 201) {
                    throw new Error('Failed!');
                }
                return res.json();
            })
            .then(resData => {
                if (resData.data.login.token) {
                    this.context.login(
                        resData.data.login.token,
                        resData.data.login.userId,
                        resData.data.login.tokenExpiration
                    );
                }
            })
            .catch(err => {
                console.log(err);
            });
    }

    render() {
        return (
            <form className="auth-form" onSubmit={this.handleSubmit}>
                <div className="form-control">
                    <label htmlFor="label">Email</label>
                    <input type="email" id="email" ref={this.emailRef} />
                </div>
                <div className="form-control">
                    <label htmlFor="password">Mật khẩu</label>
                    <input type="password" id="password" ref={this.passwordRef} />
                </div>
                <div className="form-actions">
                    <button type="submit" className="signin">
                        {this.state.isLogin ? 'Đăng nhập' : 'Đăng ký'}
                    </button>
                    <button type="button" className="signup" onClick={this.switchHandler}>
                        {this.state.isLogin ? 'Đi đến đăng ký' : 'Đi đến đăng nhập'}
                    </button>
                </div>
            </form>
        )
    }
}

export default AuthPage