import React from 'react';
import './styles.css';

const modal = props => (
    <div className="modal">
        <header className="modal-header">
            {props.title}
        </header>
        <section className="modal-content">
            <h1>{props.children}</h1>
        </section>
        <section className="modal-action">
            {props.canCancel &&
                <button className="btn" onClick={props.onCancel}>Cancel</button>}
            {props.canConfirm &&
                <button className="btn" onClick={props.onConfirm}>Confirm</button>}
        </section>
    </div>
)

export default modal;
