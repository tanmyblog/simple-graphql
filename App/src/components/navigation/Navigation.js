import React, { Fragment } from 'react';
import { NavLink } from 'react-router-dom';
import AuthContext from '../../context/auth-context';
import './styles.css';

const Navigation = props => (
    <AuthContext.Consumer>
        {(context) => (
            <header className="main-navigation">
                <div className="main-navigation-logo">
                    <h1> Booking App </h1>
                </div>
                <div className="main-navigation-item">
                    <ul>
                        {!context.token ?
                            <li><NavLink to="/auth">Login / Signup</NavLink></li>
                            :
                            <Fragment>
                                <li><NavLink to="/events">Events</NavLink></li>
                                <li><NavLink to="/bookings">Bookings</NavLink></li>
                                <li className="logout">
                                    <button onClick={context.logout}>Logout</button>
                                </li>
                            </Fragment>
                        }
                    </ul>
                </div>
            </header>
        )}
    </AuthContext.Consumer>
);

export default Navigation;
