import React, { Fragment } from 'react';
import { BrowserRouter, Route, Redirect, Switch } from 'react-router-dom';
import './App.css';

// components
import Navigation from './components/navigation';
// pages
import { BookingPage, EventPage, AuthPage } from './pages/index';
// context
import AuthContext from './context/auth-context';

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            token: null,
            userId: null,
        }
    }


    login = (token, userId, tokenExpiration) => {
        this.setState({ token, userId, tokenExpiration });
    }

    logout = () => {
        this.setState({ token: null, userId: null, tokenExpiration: null });
    }

    render() {
        return (
            <BrowserRouter>
                <AuthContext.Provider value={{
                    token: this.state.token,
                    userId: this.state.userId,
                    login: this.login,
                    logout: this.logout
                }}>
                    <Navigation />
                    <main className="main-content">
                        <Switch>
                            {!this.state.token ?
                                <Fragment>
                                    <Redirect from={'/'} to={'/auth'} exact />
                                    <Route path='/auth' component={AuthPage} />
                                </Fragment>
                                :
                                <Fragment>
                                    <Redirect from={'/'} to={'/events'} exact />
                                    <Redirect from={'/auth'} to={'/events'} exact />
                                    <Route path='/events' component={EventPage} />
                                    <Route path='/bookings' component={BookingPage} />
                                </Fragment>
                            }
                        </Switch>
                    </main>
                </AuthContext.Provider>
            </BrowserRouter >
        );
    }
}

export default App;